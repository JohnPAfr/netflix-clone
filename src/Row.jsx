import React, { useState, useEffect } from 'react';
import YouTube from 'react-youtube';
import axios from './axios';
import "./Row.css"
import movieTrailer from 'movie-trailer'

const base_url = "https://image.tmdb.org/t/p/w500"

function Row({title, fetchUrl, isLargeRow}) {
    const [movies, setMovies] = useState([]);
    const [videoId, setVideoId] = useState('');

    useEffect(() => {
            (async function() {
                const res = await axios.get(fetchUrl);
                setMovies(res.data.results);
            })()
    }, [fetchUrl]);

    const handleClick = (movie) => {
        if(videoId) {
            setVideoId('')
        } else {
            movieTrailer(movie?.name || "")
                .then((url) => {
                    const urlParams = new URLSearchParams(new URL(url).search);
                    setVideoId(urlParams.get('v'));
                }).catch(e => console.log(e))
        }
    }

    const opts = {
        height: "400",
        width: "100%",
        playerVars: {
            autoplay: 1
        }
    };

    return (
        <div className="row">
            <h2 className="row__title">{title}</h2>
            <div className="row__posters">
                {movies.map(m => <img
                    key={m.id}
                    src={base_url + (isLargeRow ? m.poster_path : m.backdrop_path)}
                    alt={m.name}
                    onClick={() => handleClick(m)}
                    className={`row__poster ${isLargeRow && 'row__posterLarge'}`}/>)}
            </div>
            { videoId && <YouTube videoId={videoId} opts={opts} />}
        </div>
    )
}

export default Row
