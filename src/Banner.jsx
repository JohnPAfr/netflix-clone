import React, { useEffect, useState } from 'react'
import axios from './axios'
import requests from './request'
import "./Banner.css"

const base_url = "https://image.tmdb.org/t/p/original"

function Banner() {
    const [movie, setMovie] = useState([]);

    useEffect(() => {
        (async function() {
            const res = await axios.get(requests.fetchNetflixOriginals);
            setMovie(res.data.results[
                Math.floor(Math.random() * res.data.results.length)
            ]);
        })()
    }, [])

    const truncate = (str, n) => {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str;
    }

    console.log('movie', movie)
    
    return (
        <header
            className="banner"
            style={{
                backgroundImage: `url(${base_url + movie?.backdrop_path})`,
                backgroundSize: "cover"
            }}>
            <div className="banner__contents">
                <h1 className='banner__title'>{movie?.name || movie?.title || movie?.original_name}</h1>
                <div className="banner__btns">
                    <button className="banner__button">Play</button>
                    <button className="banner__button">My List</button>
                </div>
                <p className="banner__overview">{truncate(movie?.overview, 150)}</p>
            </div>
            <div className="banner--fadeBottom"></div>
        </header>
    )
}

export default Banner
